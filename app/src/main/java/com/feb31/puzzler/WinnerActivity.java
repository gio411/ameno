package com.feb31.puzzler;

import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class WinnerActivity extends ActionBarActivity implements View.OnClickListener{

    int picture;
    int pictureIndex;
    private Tracker mTracker2;
    SharedPreferences prefs;
    SharedPreferences prefsTapen;
    int level;
    boolean tapToContinure;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        //=======
        ImageView imageTap=(ImageView)findViewById(R.id.tap);
        //=======

        //===================================
        //=========================
        prefsTapen=getSharedPreferences("tapen", MODE_PRIVATE);
        tapToContinure=prefsTapen.getBoolean("tapen", false);
        //==============================
        if(tapToContinure){
            imageTap.setVisibility(View.GONE);
        }
        //====================================


        Bundle extras=getIntent().getExtras();
        picture=extras.getInt("winner");
        pictureIndex=extras.getInt("pictureIndex");

        FrameLayout fLayout=(FrameLayout)findViewById(R.id.root);
        fLayout.setOnClickListener(this);
        fLayout.setBackgroundResource(picture);

        // Obtain the shared Tracker instance.
//
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker2 = application.getDefaultTracker();
//     ==
    }


    @Override
    protected void onResume() {
        super.onResume();

        prefs = getSharedPreferences("levels", MODE_PRIVATE);

        level=prefs.getInt("level", 1);; //=====================================================

        if(pictureIndex==11) { // 11
            if (level == 1) { // ?????????
//                Toast.makeText(this, "111", Toast.LENGTH_LONG).show();
//                Log.d("11111111", "11111111xxx");
                mTracker2.setScreenName("First Picture Complete");
                mTracker2.send(new HitBuilders.ScreenViewBuilder().build());
            } else if(level==2){
//                Toast.makeText(this, "222", Toast.LENGTH_LONG).show();
//                Log.d("222222222", "222222222222sdfdsfsd");
                mTracker2.setScreenName("Second Picture Complete");
                mTracker2.send(new HitBuilders.ScreenViewBuilder().build());
            }
            // mesame
//            else{
////                Toast.makeText(this, "333", Toast.LENGTH_LONG).show();
////                Log.d("333333333333333", "33333333333333333333333ggggg");
//                mTracker2.setScreenName("Thierd Picture Complete");
//                mTracker2.send(new HitBuilders.ScreenViewBuilder().build());
//            }
        }
    }

    @Override
    public void onClick(View v) {
//        getIntent().removeExtra("winner");
        //=========================

        if(!tapToContinure){
            SharedPreferences.Editor editor = getSharedPreferences("tapen", MODE_PRIVATE).edit();
            editor.putBoolean("tapen", true);
            editor.commit();

        }
        //==============================

        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getIntent().getExtras().clear();
    }

//    @Override
//    public void onDetachedFromWindow() {
//        super.onDetachedFromWindow();
//        SharedPreferences.Editor editor = getSharedPreferences("tapen", MODE_PRIVATE).edit();
//        editor.putBoolean("tapen", false);
//        editor.commit();
//    }

}
