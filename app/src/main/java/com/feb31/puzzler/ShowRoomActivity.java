package com.feb31.puzzler;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.widget.ListView;

public class ShowRoomActivity extends Activity {

    int level;
    SharedPreferences prefs;

    public int jheight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_room);

        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // deprecated
        int height = display.getHeight();

         jheight=(height*2)/5;

//        LinearLayout lro = (LinearLayout)findViewById(R.id.singlerow);
//        ViewGroup.LayoutParams params = lro.getLayoutParams();
//        params.height=height;
//        lro.setLayoutParams(params);

        prefs = getSharedPreferences("levels", MODE_PRIVATE);

        level=prefs.getInt("level", 1);; //=====================================================


        ListView listView=(ListView)findViewById(R.id.listView);
        listView.setAdapter(new VivzAdapter(this));
    }

}
