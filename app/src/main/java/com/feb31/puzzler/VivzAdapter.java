package com.feb31.puzzler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Giorgi on 8/28/2015.
 */
public class VivzAdapter extends BaseAdapter {
 ArrayList<SingleRow> list;
    Context context;

    VivzAdapter(Context c){
        context =c;
        list=new ArrayList<SingleRow>();

//        {R.drawable.tbl,R.drawable.tbl,R.drawable.tbl,R.drawable.tbl,R.drawable.tbl,R.drawable.tbl,R.drawable.tbl,R.drawable.tbl,};
        int [] images = new int [8];
        images [0] = R.drawable.tbl;
        images [1] = R.drawable.lilnasha1; // Lilnasha1
        images [2] = R.drawable.lilnasha2;
        images [3] = R.drawable.tbl; // ?
        images [4] = R.drawable.tbl;
        images [5] = R.drawable.tbl;
        images [6] = R.drawable.tbl;
        images [7] = R.drawable.tbl;

        int level = ((ShowRoomActivity)context).level;

        for(int i=1; i<9; i++){
            if(i*2 < level)
        list.add(new SingleRow(images[i*2 - 1],images[i*2]));
            else if(i*2 - 1 < level)
                list.add(new SingleRow(images[i*2 - 1],images[0]));
            else
                list.add(new SingleRow(images[0],images[0]));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.single_row, parent, false);

        ViewGroup.LayoutParams params = row.getLayoutParams();
        params.height=((ShowRoomActivity)context).jheight;
        row.setLayoutParams(params);


        ImageView image = (ImageView)row.findViewById(R.id.imageViewmeme);
        ImageView image1 = (ImageView)row.findViewById(R.id.imageView);

        SingleRow temp=list.get(i);
        image.setImageResource(temp.image);
        image1.setImageResource(temp.image1);

        return row;
    }

}


class SingleRow{

    int image;
    int image1;

    SingleRow(int image,int image1){

        this.image= image;
        this.image1= image1;

    }
}
